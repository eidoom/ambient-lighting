#include <FastLED.h>

#define LED_PIN 7
#define NUM_LEDS 122
#define BRIGHTNESS 64
#define UPS 24
#define SPEED 1

CRGB leds[NUM_LEDS];

int colourIndex = 0;
    
void setup() {
  FastLED.addLeds<WS2812B, LED_PIN, RGB>(leds, NUM_LEDS);
}

void loop() {
  colourIndex += SPEED;
  fill(colourIndex);
  FastLED.show();
  delay(1000/UPS);
}

void fill( int colorIndex) {   
    for(unsigned int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette(RainbowColors_p, colorIndex, BRIGHTNESS, LINEARBLEND);
        colorIndex += 255/NUM_LEDS;
    }
}
